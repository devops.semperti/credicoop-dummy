package com.redhat.dummy;

import org.apache.log4j.Logger;
import org.jbpm.services.task.identity.JBossUserGroupCallbackImpl;
import org.jbpm.test.JBPMHelper;
import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEnvironment;
import org.kie.api.runtime.manager.RuntimeEnvironmentBuilder;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.manager.RuntimeManagerFactory;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.UserGroupCallback;
import org.kie.internal.runtime.manager.context.EmptyContext;

import com.redhat.credicoop.handler.CommandExecuteWorkItemHandler;
import com.redhat.credicoop.params.RestServiceRequest;
import com.redhat.listeners.RedHatListener;

import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Prueba extends JbpmJUnitBaseTestCase {

    private static final Logger logger = Logger.getLogger(Prueba.class);

    @BeforeClass
    public static void setupEngine() {
        JBPMHelper.startH2Server();

        JBPMHelper.setupDataSource();

    }

    private RuntimeManager runtimeManager;
    private TaskService taskService;
    private KieSession session;
    private Object testHandler;

    @Before
    public void setup() {
        logger.info("Init setup");

        Properties users = new Properties();
        users.setProperty("diego", "admin");
        users.setProperty("adriel", "admin");

        UserGroupCallback callback = new JBossUserGroupCallbackImpl(users);

        RuntimeEnvironment environment = RuntimeEnvironmentBuilder.Factory.get()
                .newClasspathKmoduleDefaultBuilder("kbase-dummy", "ksession-process")
                .entityManagerFactory(Persistence.createEntityManagerFactory("org.jbpm.persistence.jpa"))
                .userGroupCallback(callback).get();

        runtimeManager = RuntimeManagerFactory.Factory.get().newSingletonRuntimeManager(environment);

        taskService = runtimeManager.getRuntimeEngine(EmptyContext.get()).getTaskService();

        session = runtimeManager.getRuntimeEngine(EmptyContext.get()).getKieSession();

        session.getWorkItemManager().registerWorkItemHandler("CustomItem", new CommandExecuteWorkItemHandler(session, this.getClass().getClassLoader()));
        session.addEventListener(new RedHatListener());

        logger.info("End setup");
    }

    @After
    public void tierDown() {
        logger.info("Init tier down");
        if (runtimeManager != null) {
            runtimeManager.disposeRuntimeEngine(runtimeManager.getRuntimeEngine(EmptyContext.get()));

            runtimeManager.close();
        }

        logger.info("End tier down");
    }

    @Test
    public void caseProcessAbort() {
        logger.info("Init case process abort");
        Map<String, Object> params = new HashMap<String, Object>();
        // params.put("input-command",
        // "com.redhat.credicoop.command.RestServiceCommand");
        // params.put("input-delay", "2s");
        // params.put("input-retry", 1);

        ProcessInstance pi = session.startProcess("demo", params);

        assertProcessInstanceAborted(pi.getId(), session);

        logger.info("End case process abort");
    }
}
