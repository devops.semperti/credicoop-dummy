package com.redhat.listeners;

import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jbpm.ruleflow.instance.RuleFlowProcessInstance;
import org.kie.api.event.process.ProcessCompletedEvent;
import org.kie.api.event.process.ProcessEventListener;
import org.kie.api.event.process.ProcessNodeLeftEvent;
import org.kie.api.event.process.ProcessNodeTriggeredEvent;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.process.ProcessVariableChangedEvent;
import org.kie.api.runtime.process.ProcessInstance;

public class RedHatListener implements ProcessEventListener {

	private static Logger logger = Logger.getLogger(RedHatListener.class);

	public void afterNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void afterNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void afterProcessCompleted(ProcessCompletedEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void afterProcessStarted(ProcessStartedEvent event) {
		RuleFlowProcessInstance rfpi = (RuleFlowProcessInstance) event.getProcessInstance();
		rfpi.getVariables();

		for (Entry<String, Object> entry : rfpi.getVariables().entrySet()) {
			logger.info(String.format("%s >> %s", entry.getKey(), entry.getValue()));
		}

	}

	public void afterVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNodeLeft(ProcessNodeLeftEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeNodeTriggered(ProcessNodeTriggeredEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeProcessCompleted(ProcessCompletedEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void beforeProcessStarted(ProcessStartedEvent arg0) {
		ProcessInstance instance = arg0.getProcessInstance();
		logger.info(instance.getProcessName());
	}

	public void beforeVariableChanged(ProcessVariableChangedEvent arg0) {
		// TODO Auto-generated method stub

	}

}
